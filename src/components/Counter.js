import React, { Component } from 'react'
import { connect } from 'react-redux';
import { increment, decrement, reset, sum10 } from '../actions';

class Counter extends Component {

    increaseCounter = () => {
        // remember, dispatch wants an action objetc, not a function
        this.props.increment();
    }

    decreaseCounter = () => {
        this.props.decrement();
    }

    resetCounter = () => {
        // this.props.dispatch(reset())
        this.props.reset();
    }

    sum10 = () => {
        this.props.sum10();
    }

    render() {
        return (
            <div className="counter" style={styleList.flex}>
                <button style={styleList.btnDecrease} onClick={this.decreaseCounter}>-</button>
                <span className="count" style={styleList.p} >{this.props.count}</span>
                <button style={styleList.btnIncrease} onClick={this.increaseCounter}>+</button>                
                <button style={styleList.ml3} onClick={this.resetCounter} >Reset</button>
                <button style={styleList.ml3} onClick={this.sum10} >+10</button>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
      count: state.count
    };
}

const mapDispatchToProps = {
    increment,
    decrement,
    reset,
    sum10,
}

export default connect(mapStateToProps, mapDispatchToProps)(Counter);

// Styles
const styleList = {
    flex: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center'
    },
    btnDecrease: {
        marginRight: '26px',
        background: 'red',
        color: 'white',
        fontWeight: 'bold',
        fontSize: '2rem',
        padding: '2rem'
    },
    btnIncrease: {
        marginLeft: '26px',
        background: 'blue',
        color: 'white',
        fontWeight: 'bold',
        fontSize: '2rem',
        padding: '2rem'
    },
    p: {
        fontSize: '2rem'
    },
    ml3: {
        marginLeft: '3rem'
    }
}
