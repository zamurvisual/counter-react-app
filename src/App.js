import React from 'react';
import './App.css';
import Counter from './components/Counter';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';

// REDUX
// dispatch(action) -> reducer -> new state

//Step3: create a STATE (initial state)
const counterInitialState = {
  count: 0
}

//Step2: create a REDUCER
// reducer é como array.reduce, ou idem (accumulatedValue, nextItem) => nextAccumulatedValue
// (state, action) => newState
// Important Rule of Reducers #1: Never return undefined from a reducer.
// Another rule about reducers is that they must be pure functions. This means that they can’t modify their arguments, and they can’t have side effects.
const reducer = (state = counterInitialState, action) => {
  console.log('reducer', state, action);

  //Step3: create an ACTION
  switch (action.type) {
    case 'INCREMENT':
      return {
        count: state.count + 1
      };
    case 'DECREMENT':
      return {
        count: state.count -1
      };
    case 'RESET':
      return {
        count: 0
      };
    case 'SUM10':
      return {
        count: state.count + 10
      }
    default:
      return state;
  }
}

//Step1: create a STORE
const store = createStore(
  reducer,
  applyMiddleware(thunk)
);

function App() {
  return (
    <Provider style={styleList.app} store={store}>
     <Counter />
    </Provider>
  );
}

export default App;

// Styles
const styleList = {
  app: {
    padding: '1rem'
  }
}